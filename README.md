Steps to compile Region-Templates:

1.	You will need to have OpenCV installed. We strongly suggest version 2.4.9
2.	You will need to have NScale installed to compile the examples.
3.	You will need to have MPI, Cmake, make, a suitable c/c++ compiler, libeigen2-dev, libpng-dev, libpng++-dev, libtiff 3.9.4-5ubuntu6 (through ubuntu) installed.
4.	Clone the repository (region-templates) to a folder in your machine.
5.	Open this folder.
6.	Open the terminal and go to the directory …/region-templates/runtime
7.	Create a new folder in this directory called build.
8.	Change the directory to …/region-templates/runtime/build
9.	Call ccmake using this command: “cmake ..”
10.	Ccmake might ask for the OpenCV install directory if it couldn't find it. If that is the case, please provide it.
11.	Tell ccmake to configure itself (c command inside ccmake).
12.	Change the option USE_REGION_TEMPLATES to ON.
13.	Tell ccmake to configure itself (c command inside ccmake).
14.	Change both the options BUILD_SAMPLE_APPLICATIONS and RTEMPLATES_EXAMPLES to ON.
15.	Tell ccmake to configure itself (c command inside ccmake)
16.	Press [t], and Change CMAKE_USE_RELATIVE_PATHS to ON.
17.	Tell ccmake to configure itself [c].
18.	It will now ask for nscale directory.
19.	Provide the path to nscale.
20.	Change USE_ACTIVE_HARMONY to ON.
21.	Tell it to configure itself one more time.
22.	Press [g] to generate the makefile.
23.	Now that you are back to the terminal, call make
24.	Wait for compilation/linking process to finish. 